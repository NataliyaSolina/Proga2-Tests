import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAreaSquare {

    @Test
    public void testAreaSquare () {
        Square s = new Square(0);
        Assertions.assertEquals(s.area(), 0);

        for (; s.l < 100; s.l++)
            Assertions.assertEquals(s.area(), s.l * s.l);
    }

}
