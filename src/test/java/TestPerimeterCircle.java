import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPerimeterCircle {
    @Test
    public void testPerimeterCircle () {
        Circle c = new Circle(0);
        Assertions.assertEquals(c.perimeter(), 0);

        for (; c.r < 100; c.r++)
            Assertions.assertEquals(c.perimeter(), c.r * 2 * Math.PI);
    }
}
