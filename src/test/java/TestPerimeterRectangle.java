import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPerimeterRectangle {
    @Test
    public void testPerimeterRectangle () {
        Rectangle r = new Rectangle( 0, 0);
        Assertions.assertEquals(r.perimeter(), 0);

        for (r.a = 0; r.a < 100; r.a++)
            for (r.b = 0; r.b < 100; r.b++)
                Assertions.assertEquals(r.perimeter(), (r.a + r.b) * 2);
    }
}
