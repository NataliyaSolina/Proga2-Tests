import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPerimeterSquare {

    @Test
    public void testPerimeterSquare () {
        Square s = new Square(0);
        Assertions.assertEquals(s.perimeter(), 0);

        for (; s.l < 100; s.l++)
            Assertions.assertEquals(s.perimeter(), s.l * 4);
    }
}
