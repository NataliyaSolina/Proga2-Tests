import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAreaCircle {
    @Test
    public void testAreaCircle () {
        Circle c = new Circle(0);
        Assertions.assertEquals(c.area(), 0);

       for (; c.r < 100; c.r++)
           Assertions.assertEquals(c.area(), c.r * c.r * Math.PI);
    }
}
